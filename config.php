<?php
// HTTP
define('HTTP_SERVER', 'http://opencart-test.tepee.int/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart-test.tepee.int/');

// DIR
define('DIR_APPLICATION', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/catalog/');
define('DIR_SYSTEM', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/');
define('DIR_LANGUAGE', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/catalog/language/');
define('DIR_TEMPLATE', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/catalog/view/theme/');
define('DIR_CONFIG', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/config/');
define('DIR_IMAGE', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/image/');
define('DIR_CACHE', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/cache/');
define('DIR_DOWNLOAD', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/download/');
define('DIR_UPLOAD', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/upload/');
define('DIR_MODIFICATION', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/modification/');
define('DIR_LOGS', '/Users/robsinton/Dropbox (Tepee)/GIT Repositories/opencart-test/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'opencart-test');
define('DB_PREFIX', 'oc_');
